package core.dsl

/**
 * Created by: Himanshu Patel
 * On: 25/10/2018
 * At: 12:52
 *
 * Note: small DSL for tests for better readability and test structure.
 * Each test generally has 3 parts:
 * 1. The mocking part (given)
 * 2. The execution of actions part (whenever)
 * 3. The verification of the outcome part (then)
 * This DSL is intended to assure the order and structure of each test in
 * a more readable way.
 */

object StartTest {

    /**
     * In this method we define our mocks for the current test.
     *
     * The 'given' method returns 'whenever' to enforce
     * the order of the test flow.
     */
    infix fun given(block: () -> Unit): Whenever {
        block.invoke()
        return Whenever
    }

    object Whenever {

        /**
         * In this method we state the actions that should be executed in the
         * current test.
         *
         * The 'whenever' method returns 'then' to enforce
         * the order of the test flow.
         */
        infix fun whenever(block: () -> Unit): Then {
            block.invoke()
            return Then
        }
    }

    object Then {

        /**
         * In this method we assert/verify/check the outcome.
         *
         * The 'then' method returns nothing because after the then the
         * test should have ended.
         */
        infix fun then(block: () -> Unit) = block.invoke()
    }
}
package com.himanshu.assignment.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.himanshu.assignment.R
import com.himanshu.assignment.databinding.ListItemCategoryBinding
import com.himanshu.assignment.databinding.ListItemProductBinding
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_product.view.*

/*
 * Created by Himanshu Patel
 */

/**
 * Adapter for displaying category and products in the same Recyclerview
 */

const val VIEWTYPE_CATEGORY = 0
const val VIEWTYPE_PRODUCT = 1

class ProductAdapter(private val productSelectListener: ProductSelectListener) :
    ListAdapter<ProductListItemUIModel, RecyclerView.ViewHolder>(ProductItemDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEWTYPE_CATEGORY) CategoryViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_category, parent, false
            )
        )
        else ProductViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_product, parent, false
            )
        )
    }

    override fun getItemViewType(position: Int) =
        when (getItem(position)) {
            is ProductListItemUIModel.CategoryItem -> VIEWTYPE_CATEGORY
            is ProductListItemUIModel.ProductItem -> VIEWTYPE_PRODUCT
        }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position).let { item ->
            when (holder) {
                is CategoryViewHolder -> with(holder) {
                    itemView.tag = item
                    bind(item as ProductListItemUIModel.CategoryItem)
                }
                is ProductViewHolder -> with(holder) {
                    itemView.tag = item
                    bind(createOnClickListener(item as ProductListItemUIModel.ProductItem), item)
                    Picasso.get().load(item.url).error(R.drawable.noimage)
                        .placeholder(R.drawable.noimage)
                        .into(itemView.ivProductImage)
                }
            }
        }
    }

    private fun createOnClickListener(product: ProductListItemUIModel.ProductItem): View.OnClickListener {
        return View.OnClickListener {
            productSelectListener.selectedProduct(product)
        }
    }

    /**
     * View holder for product rows
     */

    class ProductViewHolder(private val binding: ListItemProductBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            listener: View.OnClickListener,
            productListItem: ProductListItemUIModel.ProductItem
        ) {
            with(binding) {
                this.clProductData.setOnClickListener(listener)
                viewModel = productListItem
                executePendingBindings()
            }
        }
    }

    /**
     * View holder for category rows
     */

    class CategoryViewHolder(private val binding: ListItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(categoryItem: ProductListItemUIModel.CategoryItem) {
            with(binding) {
                viewModel = categoryItem
                executePendingBindings()
            }
        }
    }

    interface ProductSelectListener {
        fun selectedProduct(product: ProductListItemUIModel.ProductItem)
    }
}

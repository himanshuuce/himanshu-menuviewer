package com.himanshu.assignment.ui.main

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.himanshu.assignment.core.exception.Try
import com.himanshu.assignment.core.ext.listenTo
import com.himanshu.assignment.core.repository.Result
import com.himanshu.assignment.core.transformer.Transformer
import com.himanshu.assignment.data.repository.ProductsRepository
import com.himanshu.assignment.data.sources.db.CategoryWithProducts
import com.himanshu.assignment.ui.main.binding.MainUIEvent
import com.himanshu.assignment.ui.main.binding.MainUIEvent.Action
import com.himanshu.assignment.ui.main.binding.MainUIState
import com.himanshu.assignment.ui.main.binding.MainUIState.NavigateToDetails
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/*
 * Created by: Himanshu Patel
 */

/**
 * Main view model which interacts with data models and views
 */

class MainViewModel(
    private val repo: ProductsRepository,
    private val uiModelTransformer: Transformer<List<CategoryWithProducts>?, List<ProductListItemUIModel>>
) : ViewModel() {

    private val uiStateObservable = MediatorLiveData<MainUIState>()

    fun getStateObservable() = uiStateObservable

    fun dispatch(event: MainUIEvent) {
        when (event) {
            is MainUIEvent.Phase.OnCreateView -> getProducts()
            is Action.OnProductSelected -> {
                uiStateObservable.value =
                    NavigateToDetails(event.productItem)
            }
        }
    }

    private fun getProducts() = uiStateObservable.run {
        value = MainUIState.ShowLoading(true)
        viewModelScope.launch(Dispatchers.Main) {
            val result = repo.getProducts()
            listenTo(result) {
                value = when (it) {
                    is Result.Success -> mapRepoResult(it.data)
                    is Result.Error -> MainUIState.ShowError(it.exception)
                }
            }

        }
    }

    private fun mapRepoResult(categoryWithProductList: List<CategoryWithProducts>?) =
        uiModelTransformer.transform(categoryWithProductList).let { mapped ->
            when (mapped) {
                is Try.Success -> MainUIState.ShowProducts(mapped.data)
                is Try.Error -> MainUIState.ShowError(mapped.exception)
            }
        }
}
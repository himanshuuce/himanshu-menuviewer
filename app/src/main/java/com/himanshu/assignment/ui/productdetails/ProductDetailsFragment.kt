package com.himanshu.assignment.ui.productdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.himanshu.assignment.R
import com.himanshu.assignment.databinding.FragmentProductDetailsBinding
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_product_details.*

/*
 * Created by: Himanshu Patel
 */

const val FRAGMENT_TAG_PRODUCT_DETAILS = "FRAGMENT_TAG_PRODUCT_DETAILS"
const val BUNDLE_KEY_DETAILS = "BUNDLE_KEY_DETAILS"

/**
 * Simple fragment to show the product details
 * It gets the data using bundle arguments and displays it without any extra features.
 * It can be extended to use the MainViewModel if more features are added
 */

class ProductDetailsFragment : Fragment() {
    private val productDetailsUIModel: ProductListItemUIModel.ProductItem
        get() = requireNotNull(requireArguments().getParcelable(BUNDLE_KEY_DETAILS)) { "Product Details is required" }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val hostActivity = activity
        if (hostActivity is AppCompatActivity) {
            hostActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate view and obtain an instance of the binding class.
        val binding: FragmentProductDetailsBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false)

        // Specify the current fragment as the lifecycle owner
        binding.lifecycleOwner = this

        //assign the view model to be bound
        binding.viewModel = productDetailsUIModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Picasso.get().load(productDetailsUIModel.url).error(R.drawable.noimage)
            .placeholder(R.drawable.noimage)
            .into(ivProductDetailsImage)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Respond to the action bar's Up/Home button
        if (item.itemId == android.R.id.home) {
            goback()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun goback() {
        if (requireActivity().supportFragmentManager.backStackEntryCount > 0) {
            requireActivity().supportFragmentManager.popBackStack()
        }
    }
}
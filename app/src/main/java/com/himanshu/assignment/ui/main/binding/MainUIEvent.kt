package com.himanshu.assignment.ui.main.binding

import android.os.Bundle

/*
 * Created by: Himanshu Patel
 */

/**
 * Lists all possible UI events that need to be handled
 */

sealed class MainUIEvent {

    sealed class Phase : MainUIEvent() {
        data class OnCreateView(var args: Bundle? = null) : Phase()
    }

    sealed class Action : MainUIEvent() {
        data class OnProductSelected(val productItem: ProductListItemUIModel.ProductItem) : Action()
    }
}
package com.himanshu.assignment.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.himanshu.assignment.ui.productdetails.ProductDetailsFragment

/*
 * Created by: Himanshu Patel
 */

/**
 * New way of creating fragments
 */

class MainFragmentFactory(private val navigator: MainNavigator) : FragmentFactory() {

    override fun instantiate(classLoader: ClassLoader, className: String): Fragment =
        when (loadFragmentClass(classLoader, className)) {
            MainFragment::class.java -> MainFragment(navigator)
            ProductDetailsFragment::class.java -> ProductDetailsFragment()
            else -> super.instantiate(classLoader, className)
        }
}
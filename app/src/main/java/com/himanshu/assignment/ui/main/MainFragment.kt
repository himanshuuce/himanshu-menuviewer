package com.himanshu.assignment.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.himanshu.assignment.R
import com.himanshu.assignment.ui.main.adapter.ProductAdapter
import com.himanshu.assignment.ui.main.binding.MainUIEvent
import com.himanshu.assignment.ui.main.binding.MainUIState
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

/*
 * Created by: Himanshu Patel
 */

/**
 * Main fragment to display list of category and products
 */

const val FRAGMENT_TAG_PRODUCT_LIST = "FRAGMENT_TAG_PRODUCT_LIST"

class MainFragment(private val navigator: MainNavigator) : Fragment(),
    ProductAdapter.ProductSelectListener {
    private val viewModel by sharedViewModel<MainViewModel>()
    private lateinit var productAdapter: ProductAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val supportActionBar = (activity as AppCompatActivity).supportActionBar
        supportActionBar?.title = getString(R.string.app_name)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        savedInstanceState ?: viewModel.dispatch(MainUIEvent.Phase.OnCreateView())
        initViews()
        initObservers()
    }

    private fun initViews() {
        productAdapter = ProductAdapter(this)
        rv_products.adapter = productAdapter
    }

    private fun initObservers() {
        viewModel.getStateObservable().observe(viewLifecycleOwner, Observer { state ->
            state?.let {
                when (it) {
                    is MainUIState.ShowProducts -> showProducts(it.uiModel)
                    is MainUIState.ShowError -> showError(it.exception.localizedMessage)
                    is MainUIState.ShowLoading -> toggleLoading(it.isLoading)
                    is MainUIState.NavigateToDetails -> navigator.navigate(
                        MainNavigator.Route.ProductDetails(
                            requireActivity(),
                            it.selectedProduct
                        )
                    )
                }
            }
        })
    }

    private fun toggleLoading(isLoading: Boolean) {
        progress_bar.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun showError(errorMessage: String?) {
        rv_products.visibility = View.GONE
        toggleLoading(false)
        with(error_view) {
            visibility = View.VISIBLE
            text = errorMessage?.let { it } ?: getString(R.string.default_error_message_text)
        }
    }

    private fun showProducts(uiModel: List<ProductListItemUIModel>) {
        toggleLoading(false)
        productAdapter.submitList(uiModel)
    }

    override fun selectedProduct(product: ProductListItemUIModel.ProductItem) {
        viewModel.dispatch(MainUIEvent.Action.OnProductSelected(product))
    }
}

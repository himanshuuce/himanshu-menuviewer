package com.himanshu.assignment.ui.main.binding

/*
 * Created by: Himanshu Patel
 */

/**
 * List possible UI states that need to be handled
 */

sealed class MainUIState {

    data class NavigateToDetails(val selectedProduct: ProductListItemUIModel.ProductItem) :
        MainUIState()

    data class ShowProducts(val uiModel: List<ProductListItemUIModel>) : MainUIState()
    data class ShowError(val exception: Exception) : MainUIState()
    data class ShowLoading(val isLoading: Boolean) : MainUIState()
}
package com.himanshu.assignment.ui.main

import android.content.Context
import com.himanshu.assignment.core.livedata.SingleLiveEvent
import com.himanshu.assignment.ui.main.MainNavigator.Route.ProductDetails
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel

/*
 * Created by: Himanshu Patel
 */

/**
 * List routes and handle navigation
 */

class MainNavigator {

    sealed class Route {
        // List all the navigation routes here
        data class ProductDetails(
            val from: Context,
            val product: ProductListItemUIModel.ProductItem
        ) : Route()
    }

    private val navigationObservable = SingleLiveEvent<Route>()

    fun getNavigationObservable() = navigationObservable

    fun navigate(route: Route) {
        when (route) {
            is ProductDetails -> onNavigateToDetails(route)
        }
    }

    private fun onNavigateToDetails(
        productDetails: ProductDetails
    ) {
        navigationObservable.value = productDetails
    }

}
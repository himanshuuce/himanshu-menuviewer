package com.himanshu.assignment.ui.main.binding

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/*
 * Created by: Himanshu Patel
 */

/**
 * UI model for List item for category and product
 */

sealed class ProductListItemUIModel {
    data class CategoryItem(val name: String) : ProductListItemUIModel()
    @Parcelize
    data class ProductItem(
        val id: String,
        val name: String,
        val url: String,
        val price: String
    ) : ProductListItemUIModel(), Parcelable
}

package com.himanshu.assignment.ui.main.adapter

import androidx.recyclerview.widget.DiffUtil
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel

/*
 * Created by Himanshu on 09/06/19.
 */

class ProductItemDiffCallback : DiffUtil.ItemCallback<ProductListItemUIModel>() {
    override fun areItemsTheSame(
        oldItem: ProductListItemUIModel,
        newItem: ProductListItemUIModel
    ): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: ProductListItemUIModel,
        newItem: ProductListItemUIModel
    ): Boolean {
        return if (oldItem is ProductListItemUIModel.ProductItem && newItem is ProductListItemUIModel.ProductItem) {
            oldItem.id == newItem.id
        } else {
            (oldItem as ProductListItemUIModel.CategoryItem).name == (newItem as ProductListItemUIModel.CategoryItem).name
        }
    }
}
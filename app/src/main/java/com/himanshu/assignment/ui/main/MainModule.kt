package com.himanshu.assignment.ui.main

import com.himanshu.assignment.core.connection.Connection
import com.himanshu.assignment.data.repository.ProductsRepository
import com.himanshu.assignment.data.repository.ProductsRepositoryImpl
import com.himanshu.assignment.data.sources.api.ProductsApi
import com.himanshu.assignment.data.sources.db.AppDB
import com.himanshu.assignment.data.sources.transformer.ProductTransformer
import com.himanshu.assignment.data.transformer.ProductListItemUIModelTransformer
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/*
 * Created by: Himanshu Patel
 */

/**
 * Holds all the dependencies for the main screen
 * Dependency Injection made simple by KOIN
 */

object MainModule {

    val module = module {

        viewModel {
            MainViewModel(get(), ProductListItemUIModelTransformer())
        }
        single<ProductsRepository> {
            ProductsRepositoryImpl(
                get() as ProductsApi,
                get<AppDB>().getProductDao(),
                get() as Connection,
                ProductTransformer()
            )
        }
        factory {
            MainFragmentFactory(get())
        }
        single {
            MainNavigator()
        }
    }
}
package com.himanshu.assignment.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import com.himanshu.assignment.R
import com.himanshu.assignment.core.ext.fragmentOf
import com.himanshu.assignment.core.ext.withArguments
import com.himanshu.assignment.databinding.ActivityMainBinding
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel
import com.himanshu.assignment.ui.productdetails.BUNDLE_KEY_DETAILS
import com.himanshu.assignment.ui.productdetails.FRAGMENT_TAG_PRODUCT_DETAILS
import com.himanshu.assignment.ui.productdetails.ProductDetailsFragment
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject

/*
 * Created by: Himanshu Patel
 */

/**
 * Main Activity which holds the fragments per feature and handles navigation to different fragments
 */

class MainActivity : AppCompatActivity() {

    private val navigator by inject<MainNavigator>()

    override fun onCreate(savedInstanceState: Bundle?) {
        supportFragmentManager.fragmentFactory = get<MainFragmentFactory>()
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        setSupportActionBar(binding.toolbar)
        initObservers()
        savedInstanceState ?: gotoMainFragment()
    }

    private fun initObservers() {
        navigator.getNavigationObservable().observe(this, Observer { route ->
            when (route) {
                is MainNavigator.Route.ProductDetails -> gotoDetailsFragment(route.product)
            }
        })
    }

    private fun gotoMainFragment() {
        supportFragmentManager.commit {
            val fragment = fragmentOf(MainFragment::class)
            replace(R.id.simple_fragment_container, fragment, FRAGMENT_TAG_PRODUCT_LIST)
        }
    }

    private fun gotoDetailsFragment(productItem: ProductListItemUIModel.ProductItem) {
        supportFragmentManager.commit {
            val fragment =
                fragmentOf(ProductDetailsFragment::class).withArguments(BUNDLE_KEY_DETAILS to productItem)
            replace(
                R.id.simple_fragment_container, fragment, FRAGMENT_TAG_PRODUCT_DETAILS
            ).addToBackStack(null)
        }
    }
}

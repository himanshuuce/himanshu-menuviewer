package com.himanshu.assignment.core.ext

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

/*
 * Created by: Himanshu Patel
 */

/**
 * Extension function to deal with Mediator live data easily
 */

fun <S, T> MediatorLiveData<S>.listenTo(source: LiveData<T>?, callback: (T) -> Unit) {
    var internalSource: LiveData<T>? = null
    internalSource?.let { liveData ->
        if (liveData != source) {
            removeSource(liveData)
        }
    }
    source?.let { liveData ->
        addSource(liveData) { res ->
            internalSource = liveData
            res?.let { callback(it) }
        }
    }
}
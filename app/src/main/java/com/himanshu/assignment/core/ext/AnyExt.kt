package com.himanshu.assignment.core.ext

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/*
 * Created by: Himanshu Patel
 */

inline fun <T : Any> Any.asLiveDataSet(dataContext: MutableLiveData<T>.() -> T): LiveData<T> =
    MutableLiveData<T>().apply { value = dataContext() }

inline fun <T : Any> Any.asLiveDataPost(dataContext: MutableLiveData<T>.() -> T): LiveData<T> =
    MutableLiveData<T>().apply { postValue(dataContext()) }
package com.himanshu.assignment.core.exception

/*
 * Created by: Himanshu Patel
 */

/**
 * Makes sure Exceptions are propagated without fail
 */


sealed class Try<out O> {

    companion object {
        operator fun <O> invoke(body: () -> O): Try<O> = try {
            Success(body())
        } catch (ex: Exception) {
            Error(ex)
        }
    }

    data class Success<out O>(val data: O) : Try<O>()

    data class Error(val exception: Exception) : Try<Nothing>()
}
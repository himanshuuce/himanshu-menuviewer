package com.himanshu.assignment.core.ext

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import kotlin.reflect.KClass

/*
 * Created by: Himanshu Patel
 */

fun AppCompatActivity.navigateTo(vararg flags: Int, block: () -> Intent) =
    block().let {
        flags.forEach { arg ->
            it.addFlags(arg)
        }
        startActivity(it)
    }

/**
 * Create a fragment instance of type [T] using the fragment factory attached with the activity's fragment manager.
 * @param fragmentClass Kotlin class of the fragment
 * @return new instance of fragment [T]
 */
@Suppress("UNCHECKED_CAST")
fun <T : Fragment> FragmentActivity.fragmentOf(fragmentClass: KClass<T>): T =
    supportFragmentManager.fragmentFactory.instantiate(classLoader, fragmentClass.java.name) as T
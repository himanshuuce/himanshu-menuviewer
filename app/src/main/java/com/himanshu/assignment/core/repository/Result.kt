package com.himanshu.assignment.core.repository

/*
 * Created by: Himanshu Patel
 */

/**
 * Model for holding both success and failure result
 */

sealed class Result<out T : Any> {
    data class Success<out T : Any>(val data: T?) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
}
package com.himanshu.assignment.core.transformer

import com.himanshu.assignment.core.exception.Try

/*
 * Created by: Himanshu Patel
 */

interface Transformer<I, O> {

    fun transform(input: I?): Try<O>
}
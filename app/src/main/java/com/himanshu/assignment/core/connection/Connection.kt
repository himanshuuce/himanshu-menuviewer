package com.himanshu.assignment.core.connection

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager

/*
 * Created by: Himanshu Patel
 */

/**
 * Class to provide network connection status
 */

class Connection(private val context: Application) {

    fun isDeviceConnected() =
        (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?)?.let {
            it.activeNetworkInfo?.isConnected
        } ?: false
}
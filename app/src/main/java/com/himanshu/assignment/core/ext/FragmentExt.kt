package com.himanshu.assignment.core.ext

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import kotlin.reflect.KClass

/**
 * Convenience method to apply arguments to a fragment instance.
 * @param argumentPairs fragment arguments, as bundle key-value pairs
 * @return same fragment instance
 */
fun Fragment.withArguments(vararg argumentPairs: Pair<String, Any>): Fragment = apply {
    arguments = bundleOf(*argumentPairs)
}
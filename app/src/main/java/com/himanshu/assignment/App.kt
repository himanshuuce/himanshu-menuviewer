package com.himanshu.assignment

import android.app.Application
import com.himanshu.assignment.di.AppComponent
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level.DEBUG
import timber.log.Timber

/*
 * Created by: Himanshu Patel
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidContext(this@App)
            modules(AppComponent.modules)
            androidLogger(DEBUG)
        }
    }
}
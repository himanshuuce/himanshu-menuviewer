package com.himanshu.assignment.di

import androidx.room.Room
import com.himanshu.assignment.BuildConfig
import com.himanshu.assignment.core.connection.Connection
import com.himanshu.assignment.data.sources.api.ProductsApi
import com.himanshu.assignment.data.sources.db.AppDB
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/*
 * Created by: Himanshu Patel
 */

/**
 * Dependency Injection made simple by KOIN
 */

object AppModule {

    val module = module {

        single { Connection(androidApplication()) }

        single {
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        }

        single {
            OkHttpClient.Builder().apply {
                addInterceptor(get() as HttpLoggingInterceptor)
            }.build()
        }

        single<Retrofit> {
            Retrofit.Builder().let {
                it.baseUrl(BuildConfig.BASE_URL)
                it.addConverterFactory(GsonConverterFactory.create())
                it.client(get() as OkHttpClient)
                it.build()
            }
        }


        single<ProductsApi> { get<Retrofit>().create(ProductsApi::class.java) }

        single {
            Room.databaseBuilder(
                androidApplication(),
                AppDB::class.java,
                AppDB::class.java.simpleName
            ).build()
        }
    }
}
package com.himanshu.assignment.di

import com.himanshu.assignment.ui.main.MainModule

/*
 * Created by: Himanshu Patel
 */

/**
 * Dependency Injection made simple by KOIN
 */

object AppComponent {

    val modules = listOf(
        AppModule.module,
        MainModule.module
    )
}
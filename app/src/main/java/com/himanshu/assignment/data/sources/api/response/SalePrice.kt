package com.himanshu.assignment.data.sources.api.response

import com.google.gson.annotations.SerializedName

/*
 * Created by: Himanshu Patel
 */

/**
 * Model class for API response
 */

data class SalePrice(

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null
)
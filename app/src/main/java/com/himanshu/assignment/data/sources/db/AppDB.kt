package com.himanshu.assignment.data.sources.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

/*
 * Created by: Himanshu Patel
 */

/**
 * Room DB
 */

@Database(entities = [Category::class, Product::class], version = 1, exportSchema = false)
@TypeConverters(AppDBConverters::class)
abstract class AppDB : RoomDatabase() {
    abstract fun getProductDao(): ProductDao
}
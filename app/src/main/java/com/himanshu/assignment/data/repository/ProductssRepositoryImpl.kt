package com.himanshu.assignment.data.repository

import androidx.lifecycle.LiveData
import com.himanshu.assignment.core.connection.Connection
import com.himanshu.assignment.core.exception.Try
import com.himanshu.assignment.core.ext.asLiveDataPost
import com.himanshu.assignment.core.repository.Result
import com.himanshu.assignment.core.transformer.Transformer
import com.himanshu.assignment.data.sources.api.ProductsApi
import com.himanshu.assignment.data.sources.api.response.ProductsResponse
import com.himanshu.assignment.data.sources.db.CategoryWithProducts
import com.himanshu.assignment.data.sources.db.ProductDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/*
 * Created by: Himanshu Patel
 */

/**
 * Repository implementation.
 * Get the data to be sent to view model
 * If the device is connected to internet, get the data from the service and save it in room DB.
 * If there is no internet connection return the data from room DB.
 */

class ProductsRepositoryImpl(
    private val api: ProductsApi,
    private val productDao: ProductDao,
    private val connection: Connection,
    private val productTransformer: Transformer<List<ProductsResponse>, List<CategoryWithProducts>>
) : ProductsRepository {
    override suspend fun getProducts(): LiveData<Result<List<CategoryWithProducts>>> =
        asLiveDataPost {
            withContext(Dispatchers.IO) {
                when (connection.isDeviceConnected()) {
                    true -> fetchProductsFromApiAsync()
                    false -> getProductsFromDbAsync()
                }
            }
        }


    private suspend fun fetchProductsFromApiAsync() =

        api.getProductsAsync().let { response ->
            when (response.isSuccessful) {
                true -> mapApiResponse(response.body())
                false -> getProductsFromDbAsync()
            }
        }

    private fun getProductsFromDbAsync() =
        productDao.getProducts().let { response ->
            response?.let {
                if (response.isEmpty())
                    Result.Error(Exception("No data, please connect to internet and try again..."))
                else
                    Result.Success(it)
            } ?: Result.Error(Exception())
        }

    private fun mapApiResponse(response: List<ProductsResponse>?) =
        productTransformer.transform(response).let { mapped ->
            when (mapped) {
                is Try.Success -> Result.Success(saveToDb(mapped.data))
                is Try.Error -> Result.Error(mapped.exception)
            }
        }

    private fun saveToDb(products: List<CategoryWithProducts>?) = products?.apply {
        this.forEach {
            productDao.insert(it.category, it.products)
        }
    }
}

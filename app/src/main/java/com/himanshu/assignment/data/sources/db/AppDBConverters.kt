package com.himanshu.assignment.data.sources.db

import androidx.room.TypeConverter
import com.google.gson.Gson

/*
 * Created by: Himanshu Patel
 */

/**
 * Type converters for room DB
 */

class AppDBConverters {
    @TypeConverter
    fun fromPrice(obj: Price?) =
        obj?.let { Gson().toJson(obj) }

    @TypeConverter
    fun toPrice(json: String?) =
        json?.let { Gson().fromJson(it, Price::class.java) }
}
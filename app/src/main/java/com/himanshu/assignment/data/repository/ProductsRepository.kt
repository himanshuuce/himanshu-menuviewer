package com.himanshu.assignment.data.repository

import androidx.lifecycle.LiveData
import com.himanshu.assignment.core.repository.Result
import com.himanshu.assignment.data.sources.api.response.ProductsResponse
import com.himanshu.assignment.data.sources.db.CategoryWithProducts
import com.himanshu.assignment.data.sources.db.Product

/*
 * Created by: Himanshu Patel
 */

/**
 * Interface for repository
 */

interface ProductsRepository {

    suspend fun getProducts(): LiveData<Result<List<CategoryWithProducts>>>

}
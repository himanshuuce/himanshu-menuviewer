package com.himanshu.assignment.data.transformer

import com.himanshu.assignment.core.exception.Try
import com.himanshu.assignment.core.transformer.Transformer
import com.himanshu.assignment.data.sources.db.CategoryWithProducts
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel

/*
 * Created by: Himanshu Patel
 */

/**
 * Transforms data from DB models to UI models
 */

class ProductListItemUIModelTransformer :
    Transformer<List<CategoryWithProducts>?, List<ProductListItemUIModel>> {
    override fun transform(input: List<CategoryWithProducts>?): Try<List<ProductListItemUIModel>> {
        val returnList = ArrayList<ProductListItemUIModel>()
        input?.let {
            it.forEach {
                returnList.add(ProductListItemUIModel.CategoryItem(it.category.name))
                it.products.forEach { product ->
                    returnList.add(
                        ProductListItemUIModel.ProductItem(
                            id = product.productId,
                            name = product.name,
                            url = product.url,
                            price = FormatPriceUseCase.execute(product.price)
                        )
                    )
                }
            }

        } ?: return Try.Error(Exception())

        return Try { returnList }
    }
}
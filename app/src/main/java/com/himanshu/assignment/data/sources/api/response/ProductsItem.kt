package com.himanshu.assignment.data.sources.api.response

import com.google.gson.annotations.SerializedName

/*
 * Created by: Himanshu Patel
 */

/**
 * Model class for API response
 */

data class ProductsItem(

	@field:SerializedName("salePrice")
	val salePrice: SalePrice? = null,

	@field:SerializedName("name")
	val name: String,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: String,

	@field:SerializedName("categoryId")
	val categoryId: String,

	@field:SerializedName("url")
	val url: String
)
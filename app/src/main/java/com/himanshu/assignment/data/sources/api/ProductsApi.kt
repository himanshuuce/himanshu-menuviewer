package com.himanshu.assignment.data.sources.api

import com.himanshu.assignment.data.sources.api.response.ProductsResponse
import retrofit2.Response
import retrofit2.http.GET

/*
 * Created by: Himanshu Patel
 */

/**
 * API interface to get data from network
 */

interface ProductsApi {
    @GET("/")
    suspend fun getProductsAsync(): Response<List<ProductsResponse>>
}
package com.himanshu.assignment.data.transformer

import com.himanshu.assignment.data.sources.db.Price


/*
 * Created by: Himanshu Patel
 */

/**
 * Format price with currency symbol
 */

object FormatPriceUseCase {

    private const val CURRENCY_CODE_EURO = "EUR"
    const val CURRENCY_SYMBOL_EURO = "\u20AC "

    fun execute(
        input: Price?
    ): String =
        input?.let {
            when (input.currency) {
                CURRENCY_CODE_EURO -> CURRENCY_SYMBOL_EURO + input.amount
                else -> input.amount + " " + input.currency
            }
        } ?: ""
}


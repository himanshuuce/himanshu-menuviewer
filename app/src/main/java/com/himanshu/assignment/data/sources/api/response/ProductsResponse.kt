package com.himanshu.assignment.data.sources.api.response

import com.google.gson.annotations.SerializedName

/*
 * Created by: Himanshu Patel
 */

/**
 * Model for API response
 */

data class ProductsResponse(

	@field:SerializedName("name")
	val name: String,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: String,

	@field:SerializedName("products")
	val products: List<ProductsItem>
)
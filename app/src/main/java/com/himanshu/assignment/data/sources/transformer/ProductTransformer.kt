package com.himanshu.assignment.data.sources.transformer

import com.himanshu.assignment.BuildConfig
import com.himanshu.assignment.core.exception.Try
import com.himanshu.assignment.core.transformer.Transformer
import com.himanshu.assignment.data.sources.api.response.ProductsResponse
import com.himanshu.assignment.data.sources.api.response.SalePrice
import com.himanshu.assignment.data.sources.db.Category
import com.himanshu.assignment.data.sources.db.CategoryWithProducts
import com.himanshu.assignment.data.sources.db.Price
import com.himanshu.assignment.data.sources.db.Product

/*
 * Created by: Himanshu Patel
 */

/**
 * Transforms API response to DB models
 */

class ProductTransformer : Transformer<List<ProductsResponse>, List<CategoryWithProducts>> {

    override fun transform(input: List<ProductsResponse>?) = Try {
        input?.map { productResponse ->
            CategoryWithProducts(
                category = Category(
                    id = productResponse.id,
                    name = productResponse.name
                ), products = productResponse.products.map { productItem ->
                    Product(
                        productId = productItem.categoryId + productItem.id,
                        name = productItem.name,
                        categoryid = productItem.categoryId,
                        url = BuildConfig.BASE_URL + productItem.url,
                        price = Price(
                            amount = productItem.salePrice?.amount,
                            currency = productItem.salePrice?.currency
                        )
                    )
                }
            )
        } ?: throw Exception()
    }

}
package com.himanshu.assignment.data.sources.db

import androidx.room.*

/*
 * Created by: Himanshu Patel
 */

/**
 * Interface for room DB dao
 */

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(category: Category, products: List<Product>)

    @Transaction
    @Query("SELECT * FROM Category")
    fun getProducts(): List<CategoryWithProducts>?
}
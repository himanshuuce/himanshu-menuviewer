package com.himanshu.assignment.data.sources.db

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

/*
 * Created by: Himanshu Patel
 */

/**
 * Model classes for room DB
 */

@Entity
data class Category(
    @PrimaryKey var id: String,
    var name: String
)

@Entity
data class Product(
    @PrimaryKey var productId: String,
    var name: String,
    var categoryid: String,
    var url: String,
    var price: Price
)

data class CategoryWithProducts(
    @Embedded val category: Category,
    @Relation(
        parentColumn = "id",
        entityColumn = "categoryid"
    )
    val products: List<Product>
)

data class Price(
    val amount: String? = null,
    val currency: String? = null
)
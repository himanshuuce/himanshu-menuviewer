package com.himanshu.assignment.productdetails

import com.himanshu.assignment.R
import com.himanshu.assignment.ext.checkIfDisplayed
import com.himanshu.assignment.ext.hasText
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel

/*
 * Created by: Himanshu Patel
 */

class ProductDetailsRobot {

    fun performProductDetailsViewsChecks(uiModel: ProductListItemUIModel.ProductItem) {
        R.id.tvProductDetailsName.run { checkIfDisplayed(); hasText(uiModel.name) }
        R.id.tvProductDetailsPrice.run { checkIfDisplayed(); hasText(uiModel.price) }
    }
}
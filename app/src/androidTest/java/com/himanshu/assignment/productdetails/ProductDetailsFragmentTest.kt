package com.himanshu.assignment.productdetails

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.himanshu.assignment.R
import com.himanshu.assignment.getMockProductDetailsUIModel
import com.himanshu.assignment.singleFragmentFactory
import com.himanshu.assignment.ui.main.MainActivity
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel
import com.himanshu.assignment.ui.productdetails.BUNDLE_KEY_DETAILS
import com.himanshu.assignment.ui.productdetails.ProductDetailsFragment
import core.dsl.StartTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/*
 * Created by: Himanshu Patel
 */

@RunWith(AndroidJUnit4::class)
class ProductDetailsFragmentTest {

    @get:Rule
    val activityTestRule: ActivityTestRule<MainActivity> =
        ActivityTestRule(MainActivity::class.java, false, false)

    private val fragmentFactory = singleFragmentFactory { ProductDetailsFragment() }

    private val robot: ProductDetailsRobot by lazy { ProductDetailsRobot() }

    private fun launchFragment(fragmentFactory: FragmentFactory, args: Bundle) {
        launchFragmentInContainer<ProductDetailsFragment>(
            themeResId = R.style.AppTheme,
            factory = fragmentFactory,
            fragmentArgs = args
        )
    }

    @Test
    fun screenTest() {
        lateinit var stubUIModel: ProductListItemUIModel.ProductItem
        lateinit var stubBundle: Bundle
        StartTest given {
            stubUIModel = getMockProductDetailsUIModel()
            stubBundle = bundleOf(BUNDLE_KEY_DETAILS to stubUIModel)
        } whenever {
            launchFragment(fragmentFactory, stubBundle)
        } then {
            robot.performProductDetailsViewsChecks(stubUIModel)
        }
    }
}
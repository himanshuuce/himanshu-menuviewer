package com.himanshu.assignment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory

inline fun <reified T : Fragment> singleFragmentFactory(noinline constructor: () -> T): FragmentFactory =
    object : FragmentFactory() {
        override fun instantiate(classLoader: ClassLoader, className: String): Fragment =
            when (loadFragmentClass(classLoader, className)) {
                T::class.java -> constructor.invoke()
                else -> super.instantiate(classLoader, className)
            }
    }

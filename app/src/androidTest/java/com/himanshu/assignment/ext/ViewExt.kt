package com.himanshu.assignment.ext

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*

/*
 * Created by: Himanshu Patel
 */

fun Int.checkIfDisplayed(): ViewInteraction =
    onView(withId(this))
        .check(matches(isDisplayed()))

fun Int.hasText(textId: Int): ViewInteraction =
    onView(withId(this))
        .check(matches(withText(textId)))

fun Int.hasText(text: String): ViewInteraction =
    onView(withId(this))
        .check(matches(withText(text)))

fun Int.clickView(): ViewInteraction =
    onView(withId(this))
        .perform(click())
package com.himanshu.assignment

import com.himanshu.assignment.data.transformer.FormatPriceUseCase
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel

/*
 * Created by: Himanshu Patel
 */

const val CAT_ID = "36802"
const val PROD_ID = "1"
const val PROD_NAME = "Bread"
const val PROD_FINAL_URL = BuildConfig.BASE_URL + "/Bread.jpg"
const val PROD_AMOUNT = "0.81"


fun getMockProductDetailsUIModel(
    catId: String = CAT_ID,
    prodId: String = PROD_ID,
    prodName: String = PROD_NAME,
    prodUrl: String = PROD_FINAL_URL,
    prodAmount: String = PROD_AMOUNT
) = ProductListItemUIModel.ProductItem(
    id = catId + prodId,
    name = prodName,
    url = prodUrl,
    price = FormatPriceUseCase.CURRENCY_SYMBOL_EURO + prodAmount

)
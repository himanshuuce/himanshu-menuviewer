package com.himanshu.assignment.ui.main

import android.app.Activity
import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel
import com.himanshu.assignment.util.getMockUIModel
import com.jraska.livedata.TestObserver
import core.dsl.StartTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/*
 * Created by: Himanshu Patel
 */

class MainNavigatorTest {
    @Mock
    private lateinit var from: Activity


    private val navigator: MainNavigator by lazy {
        MainNavigator()
    }

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun onNavigateToDetailsTest_Success() {
        val mockProductItem = getMockUIModel()[1] as ProductListItemUIModel.ProductItem
        lateinit var mockRoute: MainNavigator.Route
        StartTest given {
            mockRoute = getRoute(from, mockProductItem)
        } whenever {
            navigator.navigate(mockRoute)
        } then {
            TestObserver
                .test(navigator.getNavigationObservable())
                .assertHistorySize(1)
                .assertValue(mockRoute)
        }
    }

    private fun getRoute(from: Context, product: ProductListItemUIModel.ProductItem) =
        MainNavigator.Route.ProductDetails(from, product)
}
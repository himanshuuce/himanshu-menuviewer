package com.himanshu.assignment.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.himanshu.assignment.core.exception.Try
import com.himanshu.assignment.core.ext.asLiveDataSet
import com.himanshu.assignment.core.repository.Result
import com.himanshu.assignment.data.repository.ProductsRepository
import com.himanshu.assignment.data.sources.db.CategoryWithProducts
import com.himanshu.assignment.data.transformer.ProductListItemUIModelTransformer
import com.himanshu.assignment.ui.main.binding.MainUIEvent
import com.himanshu.assignment.ui.main.binding.MainUIState
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel
import com.himanshu.assignment.util.getMockDataModel
import com.himanshu.assignment.util.getMockUIModel
import com.jraska.livedata.TestObserver
import core.CoroutinesTestRule
import core.LiveDataTestUtil
import core.dsl.StartTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.`when` as whenever

/*
 * Created by: Himanshu Patel
 */

@ExperimentalCoroutinesApi
class MainViewModelTest {
    @Mock
    private lateinit var repo: ProductsRepository
    @Mock
    private lateinit var transformer: ProductListItemUIModelTransformer


    private val viewModel: MainViewModel by lazy {
        MainViewModel(repo, transformer)
    }

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun onCreateTest_Success() {
        coroutinesTestRule.runBlockingTest {
            val mockData = getMockDataModel()
            val mockUIModel = getMockUIModel()
            whenever(repo.getProducts()).thenReturn(asLiveDataSet {
                Result.Success(
                    mockData
                )
            })
            coroutinesTestRule.pauseDispatcher()
            whenever(transformer.transform(mockData)).thenReturn(Try.Success(mockUIModel))
            viewModel.dispatch(MainUIEvent.Phase.OnCreateView())
            Assert.assertEquals(
                MainUIState.ShowLoading(true),
                LiveDataTestUtil.getValue(viewModel.getStateObservable())
            )
            coroutinesTestRule.resumeDispatcher()
            Assert.assertEquals(
                MainUIState.ShowProducts(mockUIModel),
                LiveDataTestUtil.getValue(viewModel.getStateObservable())
            )
            verify(repo).getProducts()
        }
    }

    @Test
    fun onCreateTest_Success_MapError() {
        lateinit var mockVenue: List<CategoryWithProducts>
        lateinit var exception: Exception
        StartTest given {
            mockVenue = getMockDataModel()
            exception = Exception()
        } whenever {
            whenever(runBlocking { repo.getProducts() }).thenReturn(asLiveDataSet {
                Result.Success(
                    mockVenue
                )
            })
            whenever(transformer.transform(mockVenue)).thenReturn(Try.Error(exception))
            viewModel.dispatch(MainUIEvent.Phase.OnCreateView())
        } then {
            TestObserver
                .test(viewModel.getStateObservable())
                .assertHistorySize(1)
                .assertValue(MainUIState.ShowError(exception))
        }
    }

    @Test
    fun onCreateTest_Error() {
        lateinit var exception: Exception
        StartTest given {
            exception = Exception()
        } whenever {
            whenever(runBlocking { repo.getProducts() }).thenReturn(asLiveDataSet {
                Result.Error(
                    exception
                )
            })
            viewModel.dispatch(MainUIEvent.Phase.OnCreateView())
        } then {
            verify(transformer, never()).transform(getMockDataModel())
            TestObserver
                .test(viewModel.getStateObservable())
                .assertHistorySize(1)
                .assertValue(MainUIState.ShowError(exception))
        }
    }


    @Test
    fun onProductDetailsClick_Success() {
        val mockProductItem = getMockUIModel()[1] as ProductListItemUIModel.ProductItem
        StartTest given {} whenever {
            viewModel.run {
                dispatch(MainUIEvent.Phase.OnCreateView())
                dispatch(MainUIEvent.Action.OnProductSelected(mockProductItem))
            }
        } then {
            TestObserver
                .test(viewModel.getStateObservable())
                .assertHistorySize(1)
                .assertValue(MainUIState.NavigateToDetails(mockProductItem))
        }
    }
}
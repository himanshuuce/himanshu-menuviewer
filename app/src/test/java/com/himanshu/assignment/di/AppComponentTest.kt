package com.himanshu.assignment.di

import android.app.Application
import core.dsl.StartTest
import org.junit.Before
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.koinApplication
import org.koin.test.KoinTest
import org.koin.test.check.checkModules
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/*
 * Created by: Himanshu Patel
 */

class AppComponentTest : KoinTest {

    @Mock
    private lateinit var app: Application

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun checkDependencyTreeOnKoinInit() {
        StartTest given {} whenever {} then {
            koinApplication {
                androidContext(app)
                modules(AppComponent.modules)
            }.checkModules()
        }
    }
}
package com.himanshu.assignment.util

import com.himanshu.assignment.BuildConfig
import com.himanshu.assignment.data.sources.api.response.ProductsItem
import com.himanshu.assignment.data.sources.api.response.ProductsResponse
import com.himanshu.assignment.data.sources.api.response.SalePrice
import com.himanshu.assignment.data.sources.db.Category
import com.himanshu.assignment.data.sources.db.CategoryWithProducts
import com.himanshu.assignment.data.sources.db.Price
import com.himanshu.assignment.data.sources.db.Product
import com.himanshu.assignment.data.transformer.FormatPriceUseCase
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel

const val CAT_ID = "36802"
const val CAT_NAME = "Food"
const val PROD_ID = "1"
const val PROD_NAME = "Bread"
const val PROD_URL = "/Bread.jpg"
const val PROD_FINAL_URL = BuildConfig.BASE_URL + "/Bread.jpg"
const val PROD_AMOUNT = "0.81"
const val PROD_CUR = "EUR"


fun getMockProductResponse(
    catId: String = CAT_ID,
    catName: String = CAT_NAME,
    desc: String? = "",
    prodId: String = PROD_ID,
    prodName: String = PROD_NAME,
    prodUrl: String = PROD_URL,
    prodAmount: String = PROD_AMOUNT,
    prodCur: String = PROD_CUR,
    prodDesc: String? = ""
) = listOf(
    ProductsResponse(
        name = catName,
        id = catId,
        description = desc,
        products = listOf(
            ProductsItem(
                id = prodId,
                name = prodName,
                description = prodDesc,
                url = prodUrl,
                categoryId = catId,
                salePrice = SalePrice(
                    amount = prodAmount,
                    currency = prodCur
                )
            )
        )
    )
)

fun getMockDataModel(
    catId: String = CAT_ID,
    catName: String = CAT_NAME,
    prodId: String = PROD_ID,
    prodName: String = PROD_NAME,
    prodUrl: String = PROD_FINAL_URL,
    prodAmount: String = PROD_AMOUNT,
    prodCur: String = PROD_CUR
) = listOf(
    CategoryWithProducts(
        category = Category(catId, catName),
        products = listOf(
            Product(
                catId + prodId,
                prodName,
                catId,
                prodUrl,
                Price(prodAmount, prodCur)
            )
        )
    )
)

fun getMockUIModel(
    catId: String = CAT_ID,
    catName: String = CAT_NAME,
    prodId: String = PROD_ID,
    prodName: String = PROD_NAME,
    prodUrl: String = PROD_FINAL_URL,
    prodAmount: String = PROD_AMOUNT
) = listOf(
    ProductListItemUIModel.CategoryItem(name = catName),
    ProductListItemUIModel.ProductItem(
        id = catId + prodId,
        name = prodName,
        url = prodUrl,
        price = FormatPriceUseCase.CURRENCY_SYMBOL_EURO + prodAmount
    )
)
package com.himanshu.assignment.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import com.himanshu.assignment.core.connection.Connection
import com.himanshu.assignment.core.exception.Try
import com.himanshu.assignment.core.repository.Result
import com.himanshu.assignment.data.sources.api.ProductsApi
import com.himanshu.assignment.data.sources.api.response.ProductsResponse
import com.himanshu.assignment.data.sources.db.CategoryWithProducts
import com.himanshu.assignment.data.sources.db.ProductDao
import com.himanshu.assignment.data.sources.transformer.ProductTransformer
import com.himanshu.assignment.util.getMockDataModel
import com.himanshu.assignment.util.getMockProductResponse
import com.jraska.livedata.TestObserver
import core.dsl.StartTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import retrofit2.Response
import org.mockito.Mockito.`when` as whenever

/*
 * Created by: Himanshu Patel
 */

@ExperimentalCoroutinesApi
class ProductsRepositoryTest {
    @Mock
    private lateinit var api: ProductsApi
    @Mock
    private lateinit var dao: ProductDao
    @Mock
    private lateinit var connection: Connection
    @Mock
    private lateinit var transformer: ProductTransformer

    private val repository: ProductsRepository by lazy {
        ProductsRepositoryImpl(api, dao, connection, transformer)
    }

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun getVenueTest_DeviceConnected_APISuccess() {
        lateinit var mockCatWithProd: List<CategoryWithProducts>
        lateinit var mockResponse: Response<List<ProductsResponse>>
        lateinit var data: LiveData<Result<List<CategoryWithProducts>>>
        StartTest given {
            mockCatWithProd = getMockDataModel()
            mockResponse = Response.success(getMockProductResponse())
        } whenever {
            whenever(connection.isDeviceConnected()).thenReturn(true)
            runBlocking { whenever(api.getProductsAsync()).thenReturn(mockResponse) }
            whenever(transformer.transform(any())).thenReturn(Try { mockCatWithProd })
            runBlocking { data = repository.getProducts() }
        } then {
            verify(dao, times(1)).insert(mockCatWithProd[0].category, mockCatWithProd[0].products)
            TestObserver.test(data)
                .assertValue(Result.Success(mockCatWithProd))
        }
    }

    @Test
    fun getVenueTest_DeviceConnected_APISuccess_MapError() {
        lateinit var mockCatWithProd: List<CategoryWithProducts>
        lateinit var mockResponse: Response<List<ProductsResponse>>
        lateinit var data: LiveData<Result<List<CategoryWithProducts>>>
        lateinit var exception: Exception
        StartTest given {
            mockCatWithProd = getMockDataModel()
            mockResponse = Response.success(getMockProductResponse())
            exception = Exception()
        } whenever {
            whenever(connection.isDeviceConnected()).thenReturn(true)
            runBlocking { whenever(api.getProductsAsync()).thenReturn(mockResponse) }
            whenever(transformer.transform(any())).thenReturn(Try.Error(exception))
            runBlocking { data = repository.getProducts() }
        } then {
            verify(dao, never()).insert(mockCatWithProd[0].category, mockCatWithProd[0].products)
            TestObserver.test(data)
                .assertValue(Result.Error(exception))
        }
    }

    @Test
    fun getVenueTest_DeviceConnected_APIError_DBSuccess() {
        lateinit var mockCatWithProd: List<CategoryWithProducts>
        lateinit var mockResponse: Response<List<ProductsResponse>>
        lateinit var data: LiveData<Result<List<CategoryWithProducts>>>
        StartTest given {
            mockCatWithProd = getMockDataModel()
            mockResponse =
                Response.error<List<ProductsResponse>>(500, ResponseBody.create(null, ""))
        } whenever {
            whenever(connection.isDeviceConnected()).thenReturn(true)
            runBlocking { whenever(api.getProductsAsync()).thenReturn(mockResponse) }
            whenever(dao.getProducts()).thenReturn(mockCatWithProd)
            runBlocking { data = repository.getProducts() }
        } then {
            verify(transformer, never()).transform(any())
            verify(dao, never()).insert(mockCatWithProd[0].category, mockCatWithProd[0].products)
            TestObserver.test(data)
                .assertValue(Result.Success(mockCatWithProd))
        }
    }

    @Test
    fun getVenueTest_DeviceConnected_APIError_DBError() {
        lateinit var mockCatWithProd: List<CategoryWithProducts>
        lateinit var mockResponse: Response<List<ProductsResponse>>
        lateinit var data: LiveData<Result<List<CategoryWithProducts>>>
        StartTest given {
            mockCatWithProd = getMockDataModel()
            mockResponse =
                Response.error<List<ProductsResponse>>(500, ResponseBody.create(null, ""))
        } whenever {
            whenever(connection.isDeviceConnected()).thenReturn(true)
            runBlocking { whenever(api.getProductsAsync()).thenReturn(mockResponse) }
            whenever(dao.getProducts()).thenReturn(null)
            runBlocking { data = repository.getProducts() }
        } then {
            verify(transformer, never()).transform(any())
            verify(dao, never()).insert(mockCatWithProd[0].category, mockCatWithProd[0].products)
            TestObserver.test(data)
                .assertValue { it is Result.Error }
        }
    }

    @Test
    fun getVenueTest_DeviceNotConnected_DBSuccess() {
        lateinit var mockCatWithProd: List<CategoryWithProducts>
        lateinit var data: LiveData<Result<List<CategoryWithProducts>>>
        StartTest given {
            mockCatWithProd = getMockDataModel()
        } whenever {
            whenever(connection.isDeviceConnected()).thenReturn(false)
            whenever(dao.getProducts()).thenReturn(mockCatWithProd)
            runBlocking { data = repository.getProducts() }
        } then {
            verify(transformer, never()).transform(any())
            verify(dao, never()).insert(mockCatWithProd[0].category, mockCatWithProd[0].products)
            TestObserver.test(data)
                .assertValue(Result.Success(mockCatWithProd))
        }
    }

    @Test
    fun getVenueTest_DeviceNotConnected_DBError() {
        lateinit var data: LiveData<Result<List<CategoryWithProducts>>>
        lateinit var mockCatWithProd: List<CategoryWithProducts>
        StartTest given {
            mockCatWithProd = getMockDataModel()
        } whenever {
            whenever(connection.isDeviceConnected()).thenReturn(false)
            whenever(dao.getProducts()).thenReturn(null)
            runBlocking { data = repository.getProducts() }
        } then {
            verify(transformer, never()).transform(any())
            verify(dao, never()).insert(mockCatWithProd[0].category, mockCatWithProd[0].products)
            TestObserver.test(data)
                .assertValue { it is Result.Error }
        }
    }
}
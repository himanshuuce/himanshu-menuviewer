package com.himanshu.assignment.data.sources

import com.himanshu.assignment.core.exception.Try
import com.himanshu.assignment.data.sources.api.response.ProductsResponse
import com.himanshu.assignment.data.sources.db.CategoryWithProducts
import com.himanshu.assignment.data.sources.transformer.ProductTransformer
import com.himanshu.assignment.util.getMockDataModel
import com.himanshu.assignment.util.getMockProductResponse
import core.dsl.StartTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

/*
 * Created by: Himanshu Patel
 */

class ProductsTransformerTest {
    private val transformer = ProductTransformer()

    @Test
    fun transformTest() {
        lateinit var mockInput: List<ProductsResponse>
        lateinit var mockMapped: Try<List<CategoryWithProducts>>
        lateinit var mapped: Try<List<CategoryWithProducts>>
        StartTest given {
            mockInput = getMockProductResponse()
            mockMapped = Try.Success(getMockDataModel())
        } whenever {
            mapped = transformer.transform(mockInput)
        } then {
            assertThat(mapped).isEqualTo(mockMapped)
        }
    }

    @Test
    fun transformTest_ResponseIsNull() {
        val mockInput: List<ProductsResponse>? = null
        lateinit var mapped: Try<List<CategoryWithProducts>>
        StartTest given {
        } whenever {
            mapped = transformer.transform(mockInput)
        } then {
            assertThat(mapped).matches { it is Try.Error }
        }
    }
}

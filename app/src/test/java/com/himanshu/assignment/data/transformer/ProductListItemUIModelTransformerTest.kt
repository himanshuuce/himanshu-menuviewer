package com.himanshu.assignment.data.transformer

import com.himanshu.assignment.core.exception.Try
import com.himanshu.assignment.data.sources.db.CategoryWithProducts
import com.himanshu.assignment.ui.main.binding.ProductListItemUIModel
import com.himanshu.assignment.util.getMockDataModel
import com.himanshu.assignment.util.getMockUIModel
import core.dsl.StartTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

/*
 * Created by: Himanshu Patel
 */

class ProductListItemUIModelTransformerTest {
    private val transformer: ProductListItemUIModelTransformer by lazy {
        ProductListItemUIModelTransformer()
    }

    @Test
    fun transformTest() {
        lateinit var mockInput: List<CategoryWithProducts>
        lateinit var mockMapped: Try<List<ProductListItemUIModel>>
        lateinit var mapped: Try<List<ProductListItemUIModel>>
        StartTest given {
            mockInput = getMockDataModel()
            mockMapped = Try {
                getMockUIModel()
            }
        } whenever {
            mapped = transformer.transform(mockInput)
        } then {
            assertThat(mapped).isEqualTo(mockMapped)
        }
    }


    @Test
    fun transformTest_NullInput() {
        lateinit var mapped: Try<List<ProductListItemUIModel>>
        StartTest given {} whenever {
            mapped = transformer.transform(null)
        } then {
            assertThat(mapped).matches { it is Try.Error }
        }
    }


}
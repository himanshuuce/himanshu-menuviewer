# Himanshu-MenuViewer

 A simple application which fetches a list of categories and products from a web server and displays that in a list.

 The fetched data is saved in a local database for offline support.

## Architecture
* Custom MVVM with Navigators/Events/States/UIModels/UseCases/Transformers/Delegates etc.
* The threading is done with the help of coroutines.
* Dependency injection is done with the help of Koin.
* Retrofit is used for the networking layer.
* Database is implemented using Room.
* The code is written in Kotlin.
* Used Architecture components, data binding, live data, fragment factory, view models etc.
* For testing Mockito and Espresso are used.

I have written extensive unit tests but the UI tests are written for the Product details screen due to time constraints.

